import React, {Component} from 'react';
import Auth from './features/auth/auth.component';

class App extends Component {
  render() {
    return (
      <div>
        <Auth />
      </div>
    );
  }
}

export default App;
