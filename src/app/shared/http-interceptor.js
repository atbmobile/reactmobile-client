import axios from 'axios';
import { isLoading, getHttpMessage } from './http.action';

const SYSTEM_UNAVAILABLE = 'System is currently unavailable, please try again later';
const GENERIC_ERROR = 'Something went wrong, please try again later';

const PROTECTED_ROUTES = ['account', 'accountdetails', 'bill', 'transfer'];

const isProtectedRoute = (url) => {
  const filteredList = PROTECTED_ROUTES.filter(route => {
    return url.indexOf(route) !== -1;
  })
  return filteredList.length > 0;
};

export default {
  setup: (store) => {

    axios.interceptors.request.use(
      request => {
        // console.log('Request: ', request);
        store.dispatch(isLoading(true));
        if (isProtectedRoute(request.url)) {
          if (request.method !== 'OPTIONS') {
            request.headers.authorization = `Bearer ${sessionStorage.token}`;
          }
        }
        return request;
      },
      error => {
        return Promise.reject(error);
      });

    axios.interceptors.response.use(
      response => {
        // console.log('Response: ', response);
        store.dispatch(isLoading(false));
        return response;
      },
      error => {
        // console.log('Response error: ', JSON.stringify(error));
        store.dispatch(isLoading(false));
        let errMessage = GENERIC_ERROR;
        if (error.response && error.response.data.message) {
          errMessage = error.response.data.message;
        } else if (error.message.includes('Network Error')) {
          errMessage = SYSTEM_UNAVAILABLE;
        }
        store.dispatch(getHttpMessage({error: errMessage}));
        return Promise.reject(error);
      });
  }
};
