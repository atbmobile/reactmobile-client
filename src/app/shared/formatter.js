import React, { PropTypes } from 'react';
import { FormattedNumber, IntlProvider } from 'react-intl';
import Moment from 'react-moment';

/*
  Usage:
  Import the currencyFormatter
    import {currencyFormatter} from '../util/formatter';
  Call currencyFormatter inside your render method
    {currencyFormatter(2355.45, 'CAD')}
*/

export const currencyFormatter = (amount) => {
  if (amount === undefined || amount === null) {
    // blank amount
    return (<span />);
  } else {
    return (
      <IntlProvider locale="en-ca">
        <FormattedNumber value={Math.abs(amount)} style="currency" currency="CAD" />
      </IntlProvider>
    );
  }
}


export function DateTimeLabel(props) {
  const format = props.format ? props.format : 'MMM DD, YYYY @ h:mm A';
  return <Moment format={format}>{props.date}</Moment>;
}

DateTimeLabel.propTypes = {
  date: PropTypes.instanceOf(Date),
  format: PropTypes.string,
};

export function DateLabel(props) {
  const format = props.format ? props.format : 'MMM DD, YYYY';
  return <Moment format={format}>{props.date}</Moment>;
}

DateLabel.propTypes = {
  date: PropTypes.oneOfType([
    PropTypes.instanceOf(Date),
    PropTypes.string,
  ]),
  format: PropTypes.string,
};

export function DateLabelMT(props) {
  const format = props.format ? props.format : 'MMM DD, YYYY';
  return <span><Moment tz="America/Edmonton" format={format}>{props.date}</Moment> {'MT'}</span>;
}

DateLabelMT.propTypes = {
  date: PropTypes.instanceOf(Date),
  format: PropTypes.string,
};

export function DateTimeLabelMT(props) {
  const format = props.format ? props.format : 'MMM DD, YYYY @ h:mm A';
  return <span><Moment tz="America/Edmonton" format={format}>{props.date}</Moment> {'MT'}</span>;
}

DateTimeLabelMT.propTypes = {
  date: PropTypes.instanceOf(Date),
  format: PropTypes.string,
};
