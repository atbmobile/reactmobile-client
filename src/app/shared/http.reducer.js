import { LOADING, HTTP_MESSAGE } from '../config/actionTypes'

export function loading(state = false, action) {
  switch (action.type) {
    case LOADING:
      return action.loading;
    default:
      return state;
  }
}

export function httpMessage(state = {}, action) {
  switch (action.type) {
    case HTTP_MESSAGE:
      // return { ...state, message: action.payload };
      return action.payload
    // case HTTP_ERROR:
    //   // return { error: action.payload };
    //   return action.payload;
    default:
      return state;
  }
}
