import React, { Component } from 'react';
import { connect } from 'react-redux';
import styled from 'styled-components';
import Spinner from 'react-spinkit';

const LoaderWrapper = styled(Spinner)`
  position: fixed !important;
  z-index: 999;
  height: 3em;
  width: 3em;
  margin: auto;
  top: 35% !important;
  left: 0 !important;
  right: 0;
`;

class HttpLoader extends Component {

  render() {
    if (this.props.loading) {
      return (<LoaderWrapper name="circle" fadeIn='none' color="rgb(2, 136, 209)"/>);
    } else {
      return (<div></div>);
    }
  }
}

const mapStateToProps = (state) => {
  return {
    loading: state.loading
  };
};

export default connect(mapStateToProps)(HttpLoader);