import { LOADING, HTTP_MESSAGE } from '../config/actionTypes'

export const isLoading = (isLoading) => {
  return {
    type: LOADING,
    loading: isLoading
  };
}

export const getHttpMessage = (message) => {
  return {
    type: HTTP_MESSAGE,
    payload: message
  };
}
