/**
 * Created by LAE86643 on 8/24/2017.
 */

export const AUTHENTICATED = 'authenticated';
export const LOADING = 'loading';
export const HTTP_MESSAGE = 'http_message';
export const FETCH_ACCOUNT_DETAIL = 'fetch_account_detail';
export const FETCH_ACCOUNT_SUMMARY = 'fetch_account_summary';
export const FETCH_FAVORITE_ACCOUNTS = 'fetch_favorite_account';
export const UPDATE_ACCOUNT = 'update_account';
export const SELECT_ACCOUNT = 'select_account';
export const FETCH_ACCOUNTS = 'fetch_accounts';
export const MAKE_TRANSFER = 'make_transfer';
