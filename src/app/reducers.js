/**
 * Created by LAE86643 on 8/22/2017.
 */
import { combineReducers } from 'redux';
import { routerReducer } from 'react-router-redux';
import accountDetailReducer from './features/account/details/account_detail.reducer';
import {accountSummary, selectedAccount } from './features/account/summary/account_summary.reducer';
import { reducer as formReducer } from 'redux-form';
import { authenticated } from './features/auth/auth.reducer';
import { loading, httpMessage } from './shared/http.reducer';
import transferReducer from './features/transfer/transfer.reducer'
import bills from './features/bill/bills';
import payees from './features/bill/payees';

const rootReducer = combineReducers({
  accountDetail: accountDetailReducer,
  accounts: accountSummary,
  selectedAccount: selectedAccount,
  form: formReducer,
  authenticated: authenticated,
  loading: loading,
  httpMessage: httpMessage,
  routing: routerReducer,
  transferData: transferReducer,
  bills: bills,
  payees: payees
});

export default rootReducer;
