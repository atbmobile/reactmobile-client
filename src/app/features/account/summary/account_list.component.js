import React, { Component } from 'react';
import { withRouter } from 'react-router-dom'
import { connect } from 'react-redux';
import styled from 'styled-components';
import * as actions  from './account_summary.action';
import { ListItem } from 'material-ui/List';

import FavoriteToggle from './favorite_toggle.component'
import FavoriteHeart from './favorite_heart.component'
import AccountListItem from './account_list_item.component'

const AccountItem = styled(ListItem)`
  min-width: 190px;
  margin: 0.4em !important;
  box-shadow: 7px 7px 7px #ddd;
  background-color:#fff !important;
`;

const AccountTypeHeading = styled.div`
  padding:12px;
  font-weight:bold;
  font-size: 22px;
`;

const ErrorMessage = styled.div`
  padding:12px;
  font-weight:bold;
  font-size: 22px;
  text-align: center;
`;

const FlexContainer= styled.span`
  width:100%;
  display: flex;
  flex-direction: row;
`;

let bankingAccounts = [];
let investmentAccounts = [];
let loanAccounts = [];
let creditCardAccounts = [];

class AccountList extends Component {

  componentWillMount() {
    this.props.fetchAccountSummary();
  }

  selectedAccount(account) {
    this.props.selectedAccount(account);
    this.props.history.push('/account/detail');
  }

    renderAccountList(accounts){
      return accounts.map((account) =>{
        return(
              <div key={account._id}
                   onClick ={() => this.selectedAccount(account)} >
                <AccountItem>
                  <AccountListItem account={account}/>
                </AccountItem>
              </div>
          );
      });
    }

   renderTypeHeading(accounts) {
      if (accounts.length > 0) {
       return <AccountTypeHeading>{accounts[0].type}</AccountTypeHeading>;
     }
     else {
        return;
     }
   }

   filterAccountsByType() {
     let filteredAccounts = [];
     let types = ['Banking', 'Investment', 'Loan', 'Credit Card'];
     types.forEach((type) => {
       filteredAccounts = this.props.accounts.filter((account) => {
          return account.type === type
       })

       switch (type) {
         case type = 'Banking':
             bankingAccounts = filteredAccounts;
           break;
         case type = 'Investment':
             investmentAccounts= filteredAccounts;
           break;
         case type = 'Loan':
             loanAccounts= filteredAccounts;
           break;
         case type = 'Credit Card':
             creditCardAccounts= filteredAccounts;
             break;
         default:
       }
     });
    }

    handleNoAccounts() {
      if(this.props.accounts.length === 0 )
        return <ErrorMessage> No accounts to display </ErrorMessage>
    }

    render() {
        return (
            <div>
                {this.filterAccountsByType()}
                  <FlexContainer>
                      {this.renderTypeHeading(bankingAccounts)}
                      <FavoriteToggle />
                  </FlexContainer>
                  {this.renderAccountList(bankingAccounts)}

                  {this.renderTypeHeading(investmentAccounts)}
                  {this.renderAccountList(investmentAccounts)}

                  {this.renderTypeHeading(loanAccounts)}
                  {this.renderAccountList(loanAccounts)}

                  {this.renderTypeHeading(creditCardAccounts)}
                  {this.renderAccountList(creditCardAccounts)}

                  {this.handleNoAccounts()}

            </div>
        )
    }
}

function mapStateToProps(state) {
    return { accounts: state.accounts };
}

export default withRouter(connect(mapStateToProps, actions) (AccountList));
