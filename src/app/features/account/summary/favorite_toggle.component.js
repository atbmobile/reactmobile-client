import React, { Component } from 'react';
import { Toggle } from 'material-ui';
import styled from 'styled-components';
import { connect } from 'react-redux';
import * as actions  from './account_summary.action';

const FavToggle = styled.div`
  flex:1;
  text-align:right;
  padding:12px;
`;

class FavoriteToggle extends Component {

  constructor(props) {
      super(props);
      this.state = { toggled: false };
  }

  handleFavToggle () {
    this.setState({toggled: !this.state.toggled},  () => {
      if(this.state.toggled)
          this.props.fetchFavoriteAccounts(this.props.accounts);
      else
          this.props.fetchAccountSummary();
    });
  }

  render() {
    return (
      <FavToggle>
        <Toggle
        label="Favorites View"
        toggled={this.state.toggled}
        onToggle={() => this.handleFavToggle()}
        />
      </FavToggle>
    )
  }
}

function mapStateToProps(state) {
    return { accounts: state.accounts };
}

export default connect(mapStateToProps, actions) (FavoriteToggle);
