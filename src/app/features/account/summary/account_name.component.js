import React, { Component } from 'react';
import styled from 'styled-components';
import { ChevronRight, Edit } from 'material-ui-icons';
import * as actions  from './account_summary.action';
import { connect } from 'react-redux';
import { TextField, RaisedButton  }  from 'material-ui/';

const muiStyles = {
  underlineStyle: {
    borderColor: '#0288D1',
  }
};

const EditIcon= styled(Edit)`
  max-width:20px;
  padding: 0px 8px 8px 8px ;
  flex: 1;
  color: rgb(2, 136, 209)!important;
`;

const FlexContainer= styled.span`
  width:100%;
  line-height:27px;
  display: flex;
  flex-direction: row;
`;

const Name = styled.div`
  font-weight: bold;
  min-width:150px
  flex:0;
`;

class AccountName extends Component {
    constructor(props) {
        super(props);
        this.state = { editMode: false,
                       newName: '',
                       error: '',
                       account: this.props.account};
    }

    renderName(){
      let name = this.state.account.nickname ? this.state.account.nickname : this.state.account.name;
      console.log(name);
      if(this.state.editMode) {
          return <div><TextField id="newName"
                       defaultValue={name}
                       onClick={e => e.stopPropagation()}
                       underlineFocusStyle={muiStyles.underlineStyle}
                       errorText={this.state.error}
                       onChange={e => this.setState({newName: e.target.value})} />
                </div>
      }
      else
        return <Name>{name}</Name>
    }

    handleEdit(e){
      e.stopPropagation();
      this.setState({editMode: true}, () => {
            this.renderName(this.state.account);
        });
    }

    handleSave(e){
      e.stopPropagation();

      if(!this.validateInput(this.state.newName)){
          this.setState({ account: {...this.state.account, nickname: this.state.newName}},
                         () => this.props.updateAccount(this.state.account)
        );
        this.setState({ editMode : false });
        this.setState({ error : '' });
      }
    }

    validateInput(input){
      let alphaNumeric = /^[a-zA-Z0-9 ']+$/;
      let error = false;

      if(input.trim() === ''){
        error=true;
        this.setState({error : 'Nickname cannot be blank' })
      }

      if(!input.match(alphaNumeric)){
        error=true;
        this.setState({error : 'Only letters and numbers are allowed' })
      }

      if(input.length > 30){
        error=true;
        this.setState({error : 'Nickname should be less than 30 characters long' })
      }

      return error;
    }

    renderEditIcon() {
      if(this.state.editMode)
        return  <div> <RaisedButton label="Save"
                      backgroundColor="#0288D1"
                      labelColor="#FFFFFF"
                      onClick={(e) => this.handleSave(e)}/>
                </div>
      else
        return <EditIcon onClick={(e) => this.handleEdit(e)} />
    }

    render() {
        return (
        <FlexContainer>
            {this.renderName()}
            {this.renderEditIcon()}
        </FlexContainer>
        )
    }
}
export default connect(null, actions) (AccountName);
