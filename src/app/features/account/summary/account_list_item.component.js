import React, { Component } from 'react';
import styled from 'styled-components';
import { ChevronRight } from 'material-ui-icons';
import NumberFormat from 'react-number-format';

import AccountName from './account_name.component'
import FavoriteHeart from './favorite_heart.component'

const ChevronIcon = styled(ChevronRight)`
  color: rgb(2, 136, 209)!important;
`;

const Chevron = styled.div`
  padding:5px;
  margin:5px;
  max-width:25px;
  flex: 3;
`;

const FlexContainer= styled.span`
  width:100%;
  display: flex;
  flex-direction: row;
`;

const AccountItemRight= styled.div`
  flex: 2;
  margin-left: 5px;
  min-width 100px;
`;

const AccountItemLeft= styled.div`
  flex: 1;
  min-width 100px;
`;

const AccountNumber = styled.div`
  padding-top: 0.5em;
  text-align:left;
`;

const CurrentBalance = styled.div`
  text-align: right;
  font-weight:bold;
`;

const AvailableBalance = styled.div`
  text-align: right;
  padding-top: 0.5em;
`;

const CurrencyCode = styled.span`
  text-align: right;
  text-transform: uppercase;
`;

export default class AccountListItem extends Component {

    renderAvailibleBlance(account){
       if (account.availableBalance) {
        return (
            <AvailableBalance>
              Available Balance <NumberFormat value={account.availableBalance} displayType={'text'} thousandSeparator={true} prefix={'$'} />
              <CurrencyCode> {account.currencyCode}</CurrencyCode>
            </AvailableBalance>
        );
      }
      else {
         return;
      }
    }


    render() {
      let account = this.props.account
        return (
            <FlexContainer>
                <FavoriteHeart account={account} />
                <AccountItemLeft>
                      <AccountName account={account} />
                      <AccountNumber>{account.number}</AccountNumber>
                </AccountItemLeft>
                <AccountItemRight>
                    <CurrentBalance>
                        <NumberFormat value={account.currentBalance} displayType={'text'} thousandSeparator={true} prefix={'$'} />
                        <CurrencyCode> {account.currencyCode}</CurrencyCode>
                    </CurrentBalance>
                    {this.renderAvailibleBlance(account)}
                </AccountItemRight>
                <Chevron> <ChevronIcon /> </Chevron>
            </FlexContainer>

        )
    }
}
