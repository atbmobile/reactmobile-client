import React, { Component } from 'react';
import styled from 'styled-components';
import MuiThemeProvider from 'material-ui/styles/MuiThemeProvider';
import getMuiTheme from 'material-ui/styles/getMuiTheme';
import { lightBlue700 } from 'material-ui/styles/colors';

import AccountList from './account_list.component'
import Header from './header.component';

const muiTheme = getMuiTheme({
  appBar: {
    height: 70,
    color: lightBlue700
  },
  toggle: {
    thumbOnColor: 'rgb(2, 136, 209)',
    trackOnColor: 'rgb(103, 199, 252)'
  }
});

const Accounts = styled.div`
  margin-top:60px;
  padding:10px;
`;

export default class AccountSummary extends Component {

    render() {
        return (
          <MuiThemeProvider muiTheme={muiTheme}>
            <div>
                <Header />
                <Accounts>

                  <AccountList />

                </Accounts>
            </div>
          </MuiThemeProvider >
        )
    }
}
