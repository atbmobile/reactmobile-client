import React, { Component } from 'react';
import styled from 'styled-components';
import {Checkbox} from 'material-ui';
import ActionFavorite from 'material-ui/svg-icons/action/favorite';
import ActionFavoriteBorder from 'material-ui/svg-icons/action/favorite-border';
import * as actions  from './account_summary.action';
import { connect } from 'react-redux';

const FavFlag = styled.span`
  position:0;
  padding:5px;
  margin-right:5px;
  flex: 0;
`;

class FavoriteHeart extends Component {

    constructor(props) {
        super(props);
        this.state = { account: this.props.account};
    }

    handleFavClick(event){
      event.stopPropagation();
      this.setState({ account: {...this.state.account, isFavorite: !this.props.account.isFavorite }},
                     () => this.props.updateAccount(this.state.account)
      );
    }

    render() {
        return (
            <div>
              <FavFlag>
                <Checkbox
                  checkedIcon={<ActionFavorite />}
                  uncheckedIcon={<ActionFavoriteBorder />}
                  iconStyle={{fill: '#f95172'}}
                  defaultChecked={this.props.account.isFavorite}
                  onClick={(e) => this.handleFavClick(e)}
                />
              </FavFlag>
            </div>
        )
    }
}
export default connect(null, actions) (FavoriteHeart);
