import { FETCH_ACCOUNT_SUMMARY, UPDATE_ACCOUNT, FETCH_FAVORITE_ACCOUNTS, SELECT_ACCOUNT} from 'app/config/actionTypes'

export function accountSummary( state = [], action ){

  if(!action.payload){
    return state
  }

  switch (action.type) {
        case FETCH_ACCOUNT_SUMMARY:
           state = []
           return [...state, ...action.payload] ;

        case UPDATE_ACCOUNT:
          let newState = state.map(account => account._id === action.payload._id ?
                         {...account,  "isFavorite" : action.payload.isFavorite} : account );
            return newState;

        case FETCH_FAVORITE_ACCOUNTS:
          return action.payload;

        default:
          return state;
    }
}

export function selectedAccount(state = {}, action) {
  switch (action.type) {
    case SELECT_ACCOUNT:
      return action.payload;
    default:
      return state;
  }
}
