import axios from 'axios';
import { API_URL } from 'app/config/apiConfig';
import { FETCH_ACCOUNT_SUMMARY, UPDATE_ACCOUNT, FETCH_FAVORITE_ACCOUNTS, SELECT_ACCOUNT } from 'app/config/actionTypes';


const getAccounts = (accounts) => {
  return {
    type: FETCH_ACCOUNT_SUMMARY,
    payload: accounts
  };
}

const editAccount = (account) => {
  return {
    type: UPDATE_ACCOUNT,
    payload: account
  };
}

const getFavoriteAccount = (accounts) => {
  return {
    type: FETCH_FAVORITE_ACCOUNTS,
    payload: accounts
  };
}

export function fetchAccountSummary() {
    return function action(dispatch) {
      dispatch({ type: FETCH_ACCOUNT_SUMMARY })

      const request = axios({
        method: 'GET',
        url: `${API_URL}/account/getallaccounts`
      });

      return request.then(
        response => dispatch(getAccounts(response.data))
      );
  }
}

export function updateAccount(account) {
    return function action(dispatch) {
      dispatch({ type: UPDATE_ACCOUNT })
      const request = axios({
        method: 'PUT',
        url: `${API_URL}/account/updateaccount`,
        data: {
            "accountId" : account._id,
            "isFavorite" : account.isFavorite,
            "nickname" : account.nickname
            }
      });
      return request.then(
        response => dispatch(editAccount(response.data))
      );
  }
}

export function fetchFavoriteAccounts(accounts) {

    return function action(dispatch) {
      dispatch({ type: FETCH_FAVORITE_ACCOUNTS })
      let favAccounts =[];
       accounts.map(account => {
        if(account.isFavorite)
          favAccounts.push(account)
      })
      return dispatch(getFavoriteAccount(favAccounts))
  }
}


export function selectedAccount(account) {
  return {
    type: SELECT_ACCOUNT,
    payload: account
  }
}
