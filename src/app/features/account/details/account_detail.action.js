/**
 * Created by LAE86643 on 8/21/2017.
 */
import axios from 'axios';
import { FETCH_ACCOUNT_DETAIL } from 'app/config/actionTypes';
import { API_URL } from 'app/config/apiConfig';

export function fetchAccountDetail(accountNumber) {
    const request = axios.get(`${API_URL}/transaction/${accountNumber}`);
    return {
        type: FETCH_ACCOUNT_DETAIL,
        payload: request

    }


}