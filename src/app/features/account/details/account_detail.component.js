/**
 * Created by LAE86643 on 8/21/2017.
 */

import React, {Component} from 'react';
import {connect} from 'react-redux';
import {fetchAccountDetail} from './account_detail.action';
import _ from 'lodash';
import styled from 'styled-components';
import {currencyFormatter, DateLabel} from 'app/shared/formatter';
import { ChevronLeft } from 'material-ui-icons';
import MuiThemeProvider from 'material-ui/styles/MuiThemeProvider';

const BackIcon = styled(ChevronLeft)`
  color: white !important;
  min-width: 60px;
  min-height: 60px
`;

const AccountItem = styled.div`
  display: flex;
  flex-direction: row; 
  margin-bottom: 0.2em !important;
  padding: 10px 15px 10px 0px;
  background-color: #009CDE;
 `;

const AccountItemRight = styled.div`
  margin-left: 5px;
  min-width 100px;
`;

const AccountItemLeft = styled.div`
  flex: 1;
  margin-left: 5px;
  min-width 100px;
`;

const AccountName = styled.div`
  color: white;
  font-size: 1.5em;
  font-weight: bold;
  font-family: Sentinel;
`;

const AccountNumber = styled.div`
  color: #E5F5FC;
  padding-top: 0.4em;
  font-family: 'Myriad Pro';
`;

const AccountBalance = styled.div`
  color: #E5F5FC;
  padding-top: 0.4em;
  text-align: right;
  font-family: 'Myriad Pro';
`;

const TransactionDate = styled.div`
    padding: 8px 15px;
    background-color: #c1c6c8;
    font-family: 'Myriad Pro';
`;

const TransactionDetail = styled.div`
    font-family: 'Myriad Pro';
     display: flex;
     flex-direction: row; 
     padding: 5px 15px;
`;
const Description = styled.div`
    flex: 1;
`;

const myProps = {amount: Number};
const Amount = styled('div', myProps)`
    width: 25%;
    font-weight: bolder;
    text-align: right;
    color: ${props => props.amount > 0 ? '#75B43C' : '#009CDE' };
`;


class AccountDetail extends Component {


  componentDidMount() {

    this.props.fetchAccountDetail(this.props.account.number);
  }

  renderAccountTransaction(transactions) {
    return _.map(transactions, transaction => {
      return (
        <TransactionDetail key={transaction._id}>
          <Description>{transaction.description}</Description>
          <Amount amount={transaction.amount}>
            {currencyFormatter(transaction.amount)} {transaction.amount > 0 ? 'CR' : null}
          </Amount>
        </TransactionDetail>
      )
    });
  }

  renderAccountDetail(accountTransactions) {

    if (_.size(accountTransactions) == 0) {
      return (<h2> No Transaction found </h2>);
    }
    return _.map(accountTransactions, accountTransaction => {
      return (
        <div key={accountTransaction[0].createdAt}>
          <TransactionDate><DateLabel
            date={accountTransaction[0].createdAt.substring(0, 10)}/></TransactionDate>
          { this.renderAccountTransaction(accountTransaction) }
        </div>
      );
    });
  }

  render() {

    const accountTransactions = _.groupBy(this.props.accountDetails, function (d) {
      return ( d.createdAt.substring(0, 10));
    });

    return (
      <MuiThemeProvider>
      <div>
        <AccountItem>
          <BackIcon onClick={() => this.props.history.push('/account')}/>
          <AccountItemLeft>
            <AccountName>{ this.props.account.nickName ? this.props.account.nickName : this.props.account.name } </AccountName>
            <AccountNumber>{this.props.account.number} </AccountNumber>
          </AccountItemLeft>
          <AccountItemRight>
            <AccountBalance>{currencyFormatter(this.props.account.currentBalance)}</AccountBalance>
            <AccountBalance>
              {this.props.account.availableBalance ? 'Available Balance : ' : '' }
              {this.props.account.availableBalance ? currencyFormatter(this.props.account.availableBalance) : '' }
            </AccountBalance>
          </AccountItemRight>
        </AccountItem>
        {this.renderAccountDetail(accountTransactions)}
      </div>
      </MuiThemeProvider>
    );
  }
}

function mapStateToProps(state) {

  return {
    accountDetails: state.accountDetail,
    account: state.selectedAccount
  };
}

export default connect(mapStateToProps, {fetchAccountDetail})(AccountDetail);