/**
 * Created by LAE86643 on 8/21/2017.
 */
import { FETCH_ACCOUNT_DETAIL } from 'app/config/actionTypes'

export default function( state = [], action ){

  switch (action.type) {
    case FETCH_ACCOUNT_DETAIL:
      return (action.payload.data);
  }
  return state;

}
