import React from 'react';
import PropTypes from 'prop-types';
import { Link } from 'react-router-dom';
import BillCard from './BillCard';
import { GridList } from 'material-ui/GridList';
import RaisedButton from 'material-ui/RaisedButton';

import { fetchBills } from './actions';
import { connect } from 'react-redux';
import ContentAdd from 'material-ui/svg-icons/content/add';

//TODO , deleteBill { data }
class BillsList extends React.Component {
  componentDidMount() {
    this.props.fetchBills();
  }

  render() {
    const emptyMessage = (
      <h4>There are no bills to pay in your records</h4>
    );

    // Each element becomes a GridList component containing a BillCard
    const dataList = (
      <GridList cellHeight='auto' cols={1}>
        {this.props.data.map(record => <BillCard data={record} key={record._id} />)}
      </GridList>
    );

    // Add Button at the header of the list
    return (
      <div>
        <RaisedButton style={{ marginBottom: 5 }} label="Add Payee" primary={true} fullWidth={true}
          labelPosition="before"
          icon={<ContentAdd />}
          containerElement={<Link to="/bill/new" />} />

        {this.props.data.length === 0 ? emptyMessage : dataList}
      </div>
    );
  }
}

function mapStateToProps(state) {
  return {
    data: state.bills
  }
}

export default connect(mapStateToProps, { fetchBills })(BillsList);