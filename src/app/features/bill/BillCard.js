import React from 'react';
import PropTypes from 'prop-types';
import { Link } from 'react-router-dom';
import { Card, CardActions, CardHeader, CardTitle, CardText } from 'material-ui/Card';
import DeleteIcon from 'material-ui/svg-icons/action/delete';
import MoneyIcon from 'material-ui/svg-icons/editor/attach-money';
import EditIcon from 'material-ui/svg-icons/editor/mode-edit';
import RaisedButton from 'material-ui/RaisedButton';

const BillCard = ({ data }) => (
  <Card>
    <CardHeader
      title={data.name + ' - ' + data.payee.name}
      subtitle={data.number}
      actAsExpander={true}
      showExpandableButton={true}
    >
    </ CardHeader >
    <CardText expandable={true}>
      <CardActions>
        <RaisedButton label="Pay" primary={true}
          icon={<MoneyIcon />}
          labelPosition="before" disabled={true} >
          <Link to="/bill/pay" />
        </RaisedButton>
        <RaisedButton label="Edit"
          icon={<EditIcon />}
          labelPosition="before"
          containerElement={ <Link to={ `/bill/edit/${data._id}` } /> }
        >
        </RaisedButton>
        <RaisedButton label="Delete" secondary={true}
          icon={<DeleteIcon />}
          labelPosition="before" disabled={true} >
          <Link to="/bill" />
        </RaisedButton>
      </CardActions>
    </CardText>
  </Card>
);
export default BillCard;

BillCard.propTypes = {
  data: PropTypes.object.isRequired,
  //deleteGame: React.PropTypes.func.isRequired
}