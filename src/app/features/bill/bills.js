import * as TYPES from './types';

export default function bills(state = [], action = {}) {
  switch (action.type) {
/*
    case BILL_DELETED:
      return state.filter(item => item._id !== action.billId);
*/
    case TYPES.BILL_FETCHED:
      // See if the record is already alive in the State array
      const index = state.findIndex(item => item._id === action.data._id);
      // If the record already exists then we need to replace it an return the new one
      if (index > -1) {
        return state.map(item => {
          if (item._id === action.data._id) return action.data;
          return item;
        });
      } else {
        //console.log('action.data', action.data);
        // If the record does not exist then just add it to the array
        return [
          ...state,
          action.data
        ];
      }

    case TYPES.SET_BILLS:
      return action.data;

    default: return state;
  }
}
