import React from 'react';
import styled from 'styled-components';
import Drawer from 'material-ui/Drawer';
import Card from 'material-ui/Card';
import MenuItem from 'material-ui/MenuItem';
import { Link } from 'react-router-dom';
import ExitIcon from 'material-ui/svg-icons/action/exit-to-app';
import AccountIcon from 'material-ui/svg-icons/action/account-balance';

const Logo = styled.div`
  width: 100%;
  height: 30px;
  padding: 20px;
  background-color: whitesmoke;
  border-bottom: solid 1px rgba(0,0,0,.25);
  img {
    height: 100%;
  }
`;

const CardWrapper = styled(Card)`
  box-shadow: none !important;
`;

const ListWrapper = styled.div`
  padding: 20px;
`;

const logo = require('../../../assets/images/atb-logo.png');

const SideNav = (props) => {

  const handleClose = () => {
    return props.change(false);
  }

  return (
    <Drawer
      docked={false}
      width={300}
      open={props.open}
      onRequestChange={(status) => props.change(status)} >
      <CardWrapper>
        <Logo>
          <img src={logo} alt=""/>
        </Logo>
        <ListWrapper>
          <MenuItem onTouchTap={handleClose} containerElement={<Link to="/account"/>} primaryText="Account Summary" leftIcon={ <AccountIcon/> }/>
          <MenuItem onTouchTap={handleClose} containerElement={<Link to="/"/>} primaryText="Sign Out"  leftIcon={<ExitIcon />}/>
        </ListWrapper>
      </CardWrapper>
    </Drawer>
  )
}

export default SideNav;

