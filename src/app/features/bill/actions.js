import axios from 'axios';
import { API_URL } from '../../config/apiConfig';
import * as TYPES from './types';

export function billUpdated(data) {
  return {
    type: TYPES.BILL_UPDATED,
    data
  }
}

export function updateBill(data) {
  return dispatch => {
    return axios.put(`${API_URL}/bill/${data._id}`, data)
      .then(res => dispatch(billUpdated(res.data)))
      .catch(function (error) {
        throw error.response.data.error;
      });
  }
}

export function billFetched(data) {
  if ( data.length > 0 ) data = data[0];
  return {
    type: TYPES.BILL_FETCHED,
    data
  }
}

export function fetchBill(id) {
  return dispatch => {
    const request = axios({
      method: 'GET',
      url: `${API_URL}/bill/${id}`
    });
    request.then(res => dispatch(billFetched(res.data)));
  }
}

export function addBill(data) {
  return {
    type: TYPES.ADD_BILL,
    data
  }
}

export function saveBill(data) {
  return dispatch => {
    return axios.post(`${API_URL}/bill`, data)
      .then(res => dispatch(addBill(res.data)))
      .catch(function (error) {
        throw error.response.data.error;
      });
  }
}

export function setBills(data) {
  return {
    type: TYPES.SET_BILLS,
    data
  }
}

export function fetchBills() {
  return dispatch => {
    const request = axios({
      method: 'GET',
      url: `${API_URL}/bill`
    });
    request.then(res => dispatch(setBills(res.data)));
  }
}

export function payeesFetched(data) {
  return {
    type: TYPES.SET_PAYEES,
    data
  }
}

export function fetchPayees() {
  return dispatch => {
    const request = axios({
      method: 'GET',
      url: `${API_URL}/bill/payee`
    });
    request.then(res => dispatch(payeesFetched(res.data)));
  }
}

export function billDeleted(billId) {
  return {
    type: TYPES.BILL_DELETED,
    billId
  }
}