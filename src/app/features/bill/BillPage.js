import React from 'react';
import BillsList from './BillsList';
import BillFormPage from './BillFormPage';
import { connect } from 'react-redux';
import { Route } from 'react-router-dom';
import RequireAuth from '../auth/require-auth';
import getMuiTheme from 'material-ui/styles/getMuiTheme';
import MuiThemeProvider from 'material-ui/styles/MuiThemeProvider';
import {lightBlue700, grey700, grey900 } from 'material-ui/styles/colors';
import { Card } from 'material-ui/Card';
import Header from './Header'
import styled from 'styled-components';

const FormWrapper = styled(Card)`
  padding: 15px 30px 40px;
  @media (max-width: 599px) {
    width: 100%;
    height: calc(100vh - 70px);
    margin-top: 70px;
    box-shadow: none !important;
  }
  @media (min-width: 600px) {
    width: 450px;
    margin: 100px auto;
    box-shadow: 0 3px 1px -2px rgba(0,0,0,.2), 0 2px 2px 0 rgba(0,0,0,.14), 0 1px 5px 0 rgba(0,0,0,.12) !important;
  }
`;

const muiTheme = getMuiTheme({
  appBar: {
    height: 70,
    color: lightBlue700
  },
  tabs: {
    backgroundColor: 'white',
    textColor: grey700,
    selectedTextColor: grey900
  },
  inkBar: {
    backgroundColor: lightBlue700
  },
  raisedButton: {
    primaryColor: lightBlue700
  },
  textField: {
    focusColor: lightBlue700
  },
  checkbox: {
    checkedColor: lightBlue700,
  }
});

class BillsPage extends React.Component {
  render() {
    return (
      <MuiThemeProvider muiTheme={muiTheme}>
        <div>
          <Header title="My Bills"/>
          <FormWrapper>
            <Route exact path="/bill" component={ RequireAuth( BillsList ) }/>
            <Route path="/bill/new" component={ RequireAuth( BillFormPage ) }/>
            <Route path="/bill/edit/:_id" component={ RequireAuth( BillFormPage ) }/>
          </FormWrapper>          
        </div>
      </MuiThemeProvider >
    );
  }
}

export default connect( ) ( BillsPage );
