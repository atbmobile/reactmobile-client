import React from 'react';
import { connect } from 'react-redux';
import { Redirect } from 'react-router-dom';
import { saveBill, fetchBill, updateBill, fetchPayees } from './actions';
import BillForm from './BillForm';

class BillFormPage extends React.Component {
    state = {
    redirect: false
  }

  componentDidMount = () => {
    const { match } = this.props;
    // If in edition mode, then Fetch the Bill to edit
    // Using the received _id parm
    if (match.params._id) {
      //console.log( 'this.props', this.props );
      this.props.fetchBill(match.params._id);
    } else {
      this.props.fetchPayees( );
    }
  }

  saveBill = ( { _id, name, number, payee } ) => {
    //Edition Mode
    if ( _id ) {
      return this.props.updateBill( { _id, name, number, payee } ).then(
        () => { this.setState( { redirect: true } ) },
      );
    } else {
      // New record Mode
      return this.props.saveBill( { name, number, payee } )
      .then( () => { this.setState( { redirect: true } ) }, );
    }
  }

  render() {
    // If the operation is done then just redirect to the BillsList
    return (
      <div>
        {
          this.state.redirect ?
          <Redirect to="/bill" /> :
          <BillForm
            data={ this.props.data }
            payeeDataSource={ this.props.payeeDataSource }
            saveBill={ this.saveBill }
          />
        }
      </div>
    );
  }
}

function mapStateToProps( state, props ) {
  const { match } = props;
  // When creating a new Bill we need the list of payees
  const retProps = {
    payeeDataSource: state.payees
  }
  // When an _id is received, it means that I am opening in edition mode
  // If in edition mode then, find the item in the existing States
  if (match.params._id) {
    retProps.data = state.bills.find( item => item._id == match.params._id );
  } else {
    retProps.data = {};
  }

  return retProps;
}

export default connect( mapStateToProps, { saveBill, fetchBill, updateBill, fetchPayees } ) ( BillFormPage );