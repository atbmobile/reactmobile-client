import * as TYPES from './types';

export default function payees( state = [ ], action = { } ) {
  switch( action.type ) {
    case TYPES.SET_PAYEES:
      return action.data;
  
    default: return state;
  }
}
