import React from 'react';
import _ from 'lodash';
import classnames from 'classnames';
import RaisedButton from 'material-ui/RaisedButton';
import { Link } from 'react-router-dom';
import CancelIcon from 'material-ui/svg-icons/navigation/cancel';
import SaveIcon from 'material-ui/svg-icons/content/save';
import TextField from 'material-ui/TextField';
import AutoComplete from 'material-ui/AutoComplete';

class BillForm extends React.Component {
  // I set the states in case I need default values sent as Props to this 
  // Component, in the case of an Editor mode, for example
  state = {
    _id: this.props.data ? this.props.data._id : null,
    name: this.props.data ? this.props.data.name : '',
    number: this.props.data ? this.props.data.number : '',
    payee: this.props.data ? this.props.data.payee : '',
    payeeName: this.props.data ? this.props.data.payeeName : '',
    payeeDataSource: this.props.payeeDataSource ? this.props.payeeDataSource : '',
    errors: {},
    editMode: this.props.data ? true : false,
    loading: false
  }

  // When the Form is opened in edition Mode, the state 
  // must be prepared to receive new values
  componentWillReceiveProps = (nextProps) => {
    //console.log('nextProps', nextProps );
    this.setState({
      _id: nextProps.data._id,
      name: nextProps.data.name,
      number: nextProps.data.number,
      payee: nextProps.data.payee,
      payeeDataSource: nextProps.payeeDataSource
    });
  }


  // Everytime the user changes something in the Textfields
  handleChange = (e) => {
    // If there are errors for this field, then we ned to clean the message
    // to avoid keeping the message unnecessarily 
    if (!!this.state.errors[e.target.name]) {
      let errors = Object.assign({}, this.state.errors);
      delete errors[e.target.name];
      this.setState({
        [e.target.name]: e.target.value,
        errors
      });
    } else {
      this.setState({ [e.target.name]: e.target.value });
    }
  }

  handleSubmit = (e) => {
    e.preventDefault();

    // Basic validations, just checking the required fields.
    // More advanced validations will happen in the Backend
    let errors = {};
    if (this.state.name === '') errors.name = "Please provide a name for you to remember";
    if (this.state.number === '') errors.number = "Please provide the account number associated to the bill";
    if (this.state.payee === '') errors.payee = "Please select the Payee associated to the bill";

    this.setState({ errors });
    const isValid = Object.keys(errors).length === 0

    // If no errors then I call the saving action
    if (isValid) {
      const { _id, name, number, payee } = this.state;
      this.setState({ loading: true });
      this.props.saveBill({ _id, name, number, payee })
        .catch((response) => {
          this.setState({
            errors: {
              name: _.has(response.errors, 'name.message') ? response.errors.name.message : '',
              number: _.has(response.errors, 'number.message') ? response.errors.number.message : '',
              payee: _.has(response.errors, 'payee.message') ? response.errors.payee.message : '',
            }
          })
        });
    }
  }

  handleOnNewRequest(item, index) {
    this.setState({
      payee: item._id,
      payeeName: item.name
    });
  }

  render() {
    //console.log('state', this.state);
    let PayeeField;
    // In Edition Mode I create a disabled TextField 
    if (this.state._id) {
      PayeeField = () => (
        <TextField
          disabled={true}
          value={_.has(this.state.payee, 'name') ? this.state.payee.name : ''}
          name={'payee'}
          floatingLabelText='Payee'
          errorText={this.state.errors.payee}
          fullWidth={true} />
      );
    } else {
      // An AutoComplete component will be rendered in case of
      // a New record being Inserted
      PayeeField = () => (
        <AutoComplete
          floatingLabelText='Payee name'
          filter={AutoComplete.fuzzyFilter}
          dataSource={this.state.payeeDataSource}
          dataSourceConfig={{ text: 'name', value: '_id' }}
          maxSearchResults={5}
          fullWidth={true}
          openOnFocus={true}
          onNewRequest={this.handleOnNewRequest.bind(this)}
          searchText={this.state.payeeName}
          errorText={this.state.errors.payee}
        />
      );
    }

    const form = (
      <form className={classnames({ loading: this.state.loading })} onSubmit={this.handleSubmit} >
        <h3>Add new Bill Payee</h3>

        <PayeeField />
        <TextField value={this.state.number || ''} name={'number'} floatingLabelText='Account at Payee' errorText={this.state.errors.number} fullWidth={true} onChange={this.handleChange} />
        <TextField value={this.state.name || ''} name={'name'} floatingLabelText='Custom Name' errorText={this.state.errors.name} fullWidth={true} onChange={this.handleChange} />

        <RaisedButton label="Save" primary={true}
          labelPosition="before" type={'submit'}
          icon={<SaveIcon />} />
        <RaisedButton label="Cancel"
          labelPosition="before"
          icon={<CancelIcon />}
          containerElement={<Link to="/bill" />} />
      </form>
    );
    return (
      <div>
        {form}
      </div>
    );
  }
}

export default BillForm;