export const FETCH_BILLS = 'FETCH_BILLS';
export const SET_BILLS = 'SET_BILLS';
export const ADD_BILL = 'ADD_BILL';
export const SET_PAYEES = 'SET_PAYEES';
export const BILL_FETCHED = 'BILL_FETCHED';
export const BILL_UPDATED = 'BILL_UPDATED';
export const BILL_DELETED = 'BILL_DELETED';
