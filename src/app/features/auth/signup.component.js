import React, { Component } from 'react';
import { connect } from 'react-redux';
import { withRouter } from 'react-router-dom'
import styled from 'styled-components';
import { Field, reduxForm } from "redux-form";
import { TextField, Checkbox } from 'redux-form-material-ui'
import RaisedButton from 'material-ui/RaisedButton';
import InfoIcon from 'material-ui-icons/Info';
import { signup } from './auth.action';
import { getHttpMessage } from 'app/shared/http.action';

const FormWrapper = styled.form`
  padding: 20px 10px;
`;

const TextFieldWrapper = styled.div`
  width: 100%;
  > div {
    width: 100% !important;
  }
  input:-webkit-autofill,
  input:-webkit-autofill:hover,
  input:-webkit-autofill:focus,
  input:-webkit-autofill:active {
    transition: background-color 5000s ease-in-out 0s;
    -webkit-box-shadow: 0 0 0px 1000px #fff inset;
  }
`;

const ShowPasswordWrapper = styled.div`
  margin-top: 20px;
  font-size: 14px;
  label {
    color: rgba(0, 0, 0, .7) !important;
  }
`;

const AuthButton = styled(RaisedButton)`
  width: 100%;
  height: 45px;
  margin-top: 35px;
  > button {
    height: 100% !important;
  }
  > button > div > div {
    height: 100% !important;
  }
  > button span {
    line-height: 45px;
  }
`;

const MessageWrapper = styled.div`
  display: flex;
  width: 100%;
  font-size: 14px;
  opacity: 0.7;
  padding-top: 20px;
`;

const MessageIcon = styled(InfoIcon)`
  width: 20px !important;
  color: rgb(244, 67, 54) !important;
  margin-right: 10px;
`;

const Message = styled.div`
  line-height: 24px;
  flex: 1;
`;

const VALIDATION_REQUIRED = 'Required';
const VALIDATION_MAXLENGTH = 'Maximum of 20 characters allowed';
const USERNAME_MAXLENGTH = 20;

const required = value => (value == null ? VALIDATION_REQUIRED : undefined);
const maxlength = value => (value.length > USERNAME_MAXLENGTH ? VALIDATION_MAXLENGTH : undefined);

class SignUp extends Component {

  constructor(props) {
    super(props);
    this.state = {
      passwordType: 'password'
    };
  }

  onSubmit(values) {
    this.props.signup(values);
  }

  onShowPasswordChange = (value) => {
    if (value) {
      this.setState({passwordType: 'text'});
    } else {
      this.setState({passwordType: 'password'});
    }
  }

  onFocus = () => {
    if (this.props.errMessage) {
      this.props.dispatch(getHttpMessage({error: null}));
    }
  }

  reset = () => this.props.reset();

  render() {
    const {handleSubmit, submitting} = this.props;
    return (
      <FormWrapper onSubmit={handleSubmit(this.onSubmit.bind(this))}>
        {this.props.errMessage &&
        <MessageWrapper>
          <MessageIcon></MessageIcon>
          <Message>{this.props.errMessage}</Message>
        </MessageWrapper>}
        <TextFieldWrapper>
          <Field
            name="username"
            component={TextField}
            hintText="Enter username"
            floatingLabelText="Username"
            onFocus={this.onFocus}
            validate={[required, maxlength]}
          />
        </TextFieldWrapper>
        <TextFieldWrapper>
          <Field
            name="password"
            component={TextField}
            hintText="Enter new password"
            floatingLabelText="New Password"
            onFocus={this.onFocus}
            type={this.state.passwordType}
            validate={required}
          />
        </TextFieldWrapper>
        <TextFieldWrapper>
          <Field
            name="confirmPassword"
            component={TextField}
            hintText="Enter password again"
            floatingLabelText="Confirm Password"
            onFocus={this.onFocus}
            type={this.state.passwordType}
            validate={required}
          />
        </TextFieldWrapper>
        <ShowPasswordWrapper>
          <Field
            name="showpassword"
            label="Show Password"
            onCheck={this.onShowPasswordChange}
            onFocus={this.onFocus}
            component={Checkbox}
          />
        </ShowPasswordWrapper>
        <AuthButton label='Sign Up' disabled={submitting} type="submit" primary={true}/>
      </FormWrapper>
    );
  }
}

function validate(formProps) {
  const errors = {};
  if (formProps.password !== formProps.confirmPassword) {
    errors.confirmPassword = 'Passwords must match';
  }
  return errors;
}

function mapStateToProps(state) {
  return { errMessage: state.httpMessage.error };
}

export default reduxForm({
  form: "SignUpForm",
  validate
})(withRouter(connect(mapStateToProps, {signup})(SignUp)));
