import React from 'react';
import styled from 'styled-components';
import Drawer from 'material-ui/Drawer';
import Card from 'material-ui/Card';
import MenuItem from 'material-ui/MenuItem';
// import { Link } from 'react-router-dom';

const Logo = styled.div`
  width: 100%;
  height: 30px;
  padding: 20px;
  background-color: whitesmoke;
  border-bottom: solid 1px rgba(0,0,0,.25);
  img {
    height: 100%;
  }
`;

const CardWrapper = styled(Card)`
  box-shadow: none !important;
`;

const ListWrapper = styled.div`
  padding: 20px;
`;

const logo = require('./atb-logo.png');

const SideNav = (props) => {

  const handleClose = () => {
    return props.change(false);
  }

  return (
    <Drawer
      docked={false}
      width={300}
      open={props.open}
      // containerStyle={{top: 70}}
      onRequestChange={(status) => props.change(status)}
    >
      <CardWrapper>
        <Logo>
          <img src={logo} alt=""/>
        </Logo>
        <ListWrapper>
          {/*<MenuItem onTouchTap={handleClose} containerElement={<Link to="/findbranch"/>} primaryText="Find Branch"/>*/}
          {/*<MenuItem onTouchTap={handleClose} containerElement={<Link to="/rates"/>} primaryText="Rates"/>*/}
          {/*<MenuItem onTouchTap={handleClose} containerElement={<Link to="/moreinfo"/>} primaryText="More Info"/>*/}
          <MenuItem onTouchTap={handleClose} primaryText="Find Branch"/>
          <MenuItem onTouchTap={handleClose} primaryText="Rates"/>
          <MenuItem onTouchTap={handleClose} primaryText="More Info"/>
        </ListWrapper>
      </CardWrapper>
    </Drawer>
  )
}

export default SideNav;

