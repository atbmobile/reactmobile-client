import React, { Component } from 'react';
import { connect } from 'react-redux';
import { getHttpMessage } from 'app/shared/http.action';

export default function(ComposedComponent) {

  class RequireAuth extends Component {

    componentWillMount() {
      if (! this.props.authenticated) {
        this.props.history.push(this.getRedirectPath());
      }
    };

    componentWillUpdate(nextProps) {
      if (! nextProps.authenticated) {
        this.props.history.push(this.getRedirectPath());
      }
    };

    getRedirectPath = () => {
      return {
        pathname: '/signin',
        state: {
          from: {pathname: this.props.location.pathname}
        }
      }
    }

    render() {
      if (this.props.authenticated) {
        return <ComposedComponent {...this.props} />
      } else {
        return <div></div>;
      }
    }
  }

  function mapStateToProps(state) {
    return { authenticated: state.authenticated };
  }

  return connect(mapStateToProps)(RequireAuth);
}