import axios from "axios";
import { push } from 'react-router-redux';
import { API_URL } from 'app/config/apiConfig';
import { AUTHENTICATED } from 'app/config/actionTypes';
import { getHttpMessage } from 'app/shared/http.action';

const ACCOUNT_SUMMARY_URL = '/account';
const SIGNIN_URL = '/signin';

export function isAuthenticated(isAuthenticated) {
  return {
    type: AUTHENTICATED,
    authenticated: isAuthenticated
  };
}

export function signin({username, password}) {
  return function(dispatch, getState) {
    axios.post(`${API_URL}/auth/signin`, {username, password})
      .then(response => {
        dispatch(isAuthenticated(true));
        sessionStorage.setItem('token', response.data.token);
        dispatch(getHttpMessage({error: null}));
        const redirectUrl = getState().routing.location.state && getState().routing.location.state.from ?
          getState().routing.location.state.from.pathname : ACCOUNT_SUMMARY_URL;
        dispatch(push(redirectUrl));
      })
  }
}

export function signup({username, password}) {
  return function(dispatch) {
    axios.post(`${API_URL}/auth/signup`, {username, password})
      .then(response => {
        dispatch(getHttpMessage({error: null}));
        dispatch(push(SIGNIN_URL));
      })
  }
}

export function resetPassword({username, password}) {
  return function(dispatch) {
    axios.post(`${API_URL}/auth/resetpassword`, {username, password})
      .then(response => {
        dispatch(getHttpMessage({error: null}));
        dispatch(push(SIGNIN_URL));
      })
  }
}