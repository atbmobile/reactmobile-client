import { AUTHENTICATED } from 'app/config/actionTypes'

export function authenticated(state = false, action) {
  switch (action.type) {
    case AUTHENTICATED:
      return action.authenticated;
    default:
      return state;
  }
}