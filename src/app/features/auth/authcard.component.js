import React from 'react';
import { withRouter } from 'react-router-dom'
import { connect } from 'react-redux';
import styled from 'styled-components';
import { Card } from 'material-ui/Card';
import { Tabs, Tab } from 'material-ui/Tabs';
import SwipeableViews from 'react-swipeable-views';

import SignIn from './signin.component'
import SignUp from './signup.component'
import ResetPassword from './resetpassword.component'
import { getHttpMessage } from 'app/shared/http.action';

const AuthCardWrapper = styled(Card)`
  padding: 15px 30px 40px;
  @media (max-width: 599px) {
    width: 100%;
    height: calc(100vh - 70px);
    margin-top: 70px;
    box-shadow: none !important;
  }
  @media (min-width: 600px) {
    width: 450px;
    margin: 100px auto;
    box-shadow: 0 3px 1px -2px rgba(0,0,0,.2), 0 2px 2px 0 rgba(0,0,0,.14), 0 1px 5px 0 rgba(0,0,0,.12) !important;
  }
`;

const ResetPasswordWrapper = styled(Card)`
  @media (max-width: 599px) {
    width: 100%;
    height: calc(100vh - 70px);
    margin-top: 70px;
    box-shadow: none !important;
  }
  @media (min-width: 600px) {
    width: 450px;
    margin: 100px auto;
    box-shadow: 0 3px 1px -2px rgba(0,0,0,.2), 0 2px 2px 0 rgba(0,0,0,.14), 0 1px 5px 0 rgba(0,0,0,.12) !important;
  }
`;

const SIGNIN_URL = '/signin';
const SIGNUP_URL = '/signup';

class AuthCard extends React.Component {

  componentWillMount() {
    this.state = {
      curPage: 'signin',
      slideIndex: 0
    };
    this.onUrlChange(this.props.location);
    this.unlisten = this.props.history.listen((location, action) => {
      this.onUrlChange(location);
    });
  }

  componentWillUnmount() {
    this.unlisten();
  }

  onUrlChange = (location) => {
    if (location.pathname.indexOf('signin') !== -1) {
      this.setState({curPage: 'signin', slideIndex: 0});
    } else if (location.pathname.indexOf('signup') !== -1) {
      this.setState({curPage: 'signup', slideIndex: 1});
    } else if (location.pathname.indexOf('resetpassword') !== -1) {
      this.setState({curPage: 'resetpassword'});
    }
    this.props.dispatch(getHttpMessage({error: null}));
  };

  handleChange = (value) => {
    this.setState({
      slideIndex: value,
    });
    if (this.state.slideIndex === 0) {
      this.props.history.push(SIGNUP_URL);
      this.refs.signinForm.reset();
    } else {
      this.props.history.push(SIGNIN_URL);
      this.refs.signupForm.reset();
    }
    this.props.dispatch(getHttpMessage({error: null}));
  };

  render() {
    if (this.state.curPage !== 'resetpassword') {
      return (
        <AuthCardWrapper>
          <Tabs
            contentContainerStyle={{borderBottom: 'solid 1px rgba(0, 0, 0, .25)'}}
            inkBarStyle={{height: '3px'}}
            onChange={this.handleChange}
            value={this.state.slideIndex}
          >
            <Tab label="Sign In" value={0} />
            <Tab label="Sign Up" value={1} />
          </Tabs>
          <SwipeableViews
            index={this.state.slideIndex}
            onChangeIndex={this.handleChange}
          >
            <div>
              <SignIn ref="signinForm"></SignIn>
            </div>
            <div>
              <SignUp ref="signupForm"></SignUp>
            </div>
          </SwipeableViews>
        </AuthCardWrapper>
      );
    } else {
      return (
        <ResetPasswordWrapper>
          <ResetPassword></ResetPassword>
        </ResetPasswordWrapper>
      );
    }

  }
}

export default withRouter(connect()(AuthCard));
