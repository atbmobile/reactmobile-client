import React, {Component} from 'react';
import getMuiTheme from 'material-ui/styles/getMuiTheme';
import MuiThemeProvider from 'material-ui/styles/MuiThemeProvider';
import {lightBlue700, grey700, grey900 } from 'material-ui/styles/colors';
import injectTapEventPlugin from 'react-tap-event-plugin';

import Header from './header.component'
import TransferForm from './transferform.component'

// injectTapEventPlugin();

const muiTheme = getMuiTheme({
    appBar: {
        height: 70,
        color: lightBlue700
    },
    tabs: {
        backgroundColor: 'white',
        textColor: grey700,
        selectedTextColor: grey900
    },
    inkBar: {
        backgroundColor: lightBlue700
    },
    raisedButton: {
        primaryColor: lightBlue700
    },
    textField: {
        focusColor: lightBlue700
    },
    checkbox: {
        checkedColor: lightBlue700,
    }
});

class Transfer extends Component {

    render() {
        return (
            <MuiThemeProvider muiTheme={muiTheme}>
                <div>
                    <Header title="ATB Mobile"/>
                    <TransferForm></TransferForm>
                </div>
            </MuiThemeProvider>
        )
    }
}

export default Transfer;