import React, {Component} from 'react';
import getMuiTheme from 'material-ui/styles/getMuiTheme';
import MuiThemeProvider from 'material-ui/styles/MuiThemeProvider';
import {lightBlue700, grey700, grey900 } from 'material-ui/styles/colors';

import Header from './header.component';
import FailedDetails from './faileddetail.component';

import styled from 'styled-components';

const FailedDetail = styled.div`
  margin-top:60px;
  padding:10px;
`;

const muiTheme = getMuiTheme({
    appBar: {
        height: 70,
        color: lightBlue700
    },
    tabs: {
        backgroundColor: 'white',
        textColor: grey700,
        selectedTextColor: grey900
    },
    inkBar: {
        backgroundColor: lightBlue700
    },
    raisedButton: {
        primaryColor: lightBlue700
    },
    textField: {
        focusColor: lightBlue700
    },
    checkbox: {
        checkedColor: lightBlue700,
    }
});

class TransferFailed extends Component {

    render() {
        return (
            <div>
            <MuiThemeProvider muiTheme={muiTheme}>
                    <Header title="ATB Mobile"/>
            </MuiThemeProvider>
                <FailedDetail><h2>Transfer Failed</h2></FailedDetail>
            </div>

        )
    }
}

export default TransferFailed;

