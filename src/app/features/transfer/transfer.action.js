/**
 * Created by E75536 on 8/25/2017.
 */

import axios from "axios";
import { API_URL } from "../../config/apiConfig";
import { FETCH_ACCOUNTS,  MAKE_TRANSFER } from '../../config/actionTypes';
import { getHttpMessage } from 'app/shared/http.action';
import { push } from 'react-router-redux';
//const TOKEN = ""; // "Bearer eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJ1c2VybmFtZSI6IjExMTEiLCJ1c2VySWQiOjEsInJvbGUiOiJ1c2VyIiwiZXhwIjoxNTA0OTgwNDA3LCJpYXQiOjE1MDQ4OTQwMDd9.Gq_R87BSzv1ENiBgxpIXaxd18QR_Wth5u70kuVpxC84";

const getAccounts = (accounts) => {
    return {
        type: FETCH_ACCOUNTS,
        payload: accounts
    };
}

const transferData = (data) => {
    return {
        type: MAKE_TRANSFER,
        payload: data
    };
}

export function fetchAccounts() {
    return function action(dispatch) {

        const request = axios({
            method: 'GET',
            url: `${API_URL}/account/getallaccounts`
        });

        return request.then(
            response => dispatch(getAccounts(response.data))
        );
    }
}

export function makeTransfer({fromNumber, toNumber, amount}) {
    return function action(dispatch) {
        axios.post(`${API_URL}/transfer/makeTransfer`, {fromNumber, toNumber, amount})
            .then(response =>{
                dispatch(getHttpMessage({error: null}));
                dispatch(transferData(response.data))
            })
            .catch((response) => {
                dispatch(push('/transferfailed'));
            });
    }
}

