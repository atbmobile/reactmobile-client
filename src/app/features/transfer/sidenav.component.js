import React from 'react';
import styled from 'styled-components';
import Drawer from 'material-ui/Drawer';
import Card from 'material-ui/Card';
import { Link } from 'react-router-dom';

const CardWrapper = styled(Card)`
  box-shadow: none !important;
`;

const ListWrapper = styled.div`
  padding: 20px;
`;



const SideNav = (props) => {


  return (
    <Drawer
      docked={false}
      width={300}
      open={props.open}
      onRequestChange={(status) => props.change(status)}
    >
      <CardWrapper>
        <ListWrapper>
          <p><Link to={'account'}>Account Summary</Link></p>
          <p><Link to={'bill'}>My Bills</Link></p>
        </ListWrapper>
      </CardWrapper>
    </Drawer>
  )
}

export default SideNav;
