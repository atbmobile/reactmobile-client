import { combineReducers } from "redux";
import { reducer as formReducer } from "redux-form";
import TransferReducer from "./transfer.reducer";

const rootReducer = combineReducers({
    transfer: TransferReducer,
    form: formReducer
});

export default rootReducer;
