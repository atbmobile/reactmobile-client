import React, { Component } from 'react';
import AppBar from 'material-ui/AppBar';
import styled from 'styled-components';
import SideNav from './sidenav.component';
import FlatButton from 'material-ui/FlatButton'

const AccountsNav = styled.div`
  position: fixed;
  top: 0;
  width: 100%;
  z-index: 10;
`;

class Header extends Component {

  constructor(props) {
    super(props);
    this.state = {
      drawer: false
    };
    this.handleDrawerToggle = this.handleDrawerToggle.bind(this);
    this.handleDrawerChange = this.handleDrawerChange.bind(this);
  }

  handleDrawerToggle() {
    this.setState({ sidenav: ! this.state.sidenav });
  }

  handleDrawerChange( status ) {
    this.setState({ sidenav: status });
  }

  handleSignOut() {
    sessionStorage.removeItem('token');
  }

  render() {
    return (
      <AccountsNav>
      <AppBar
      title="Make Transfer"
      onLeftIconButtonTouchTap={ this.handleDrawerToggle }
      iconElementRight={<FlatButton  label="Sign Out" onTouchTap={this.handleSignOut} href='/'/>}
      />
      <SideNav open={ this.state.sidenav } change={ this.handleDrawerChange } />
    </AccountsNav>
    )
  }
}

export default Header;
