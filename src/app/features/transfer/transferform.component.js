/**
 * Created by E75536 on 8/24/2017.
 */

import _ from "lodash";
import React, { Component } from 'react';
import { Field, reduxForm } from 'redux-form';
import * as actions from "./transfer.action";
import { connect } from "react-redux";


import getMuiTheme from 'material-ui/styles/getMuiTheme';
import {lightBlue700} from 'material-ui/styles/colors';
import { Card } from 'material-ui/Card';
import styled from 'styled-components';

import TextField from 'material-ui/TextField';
import SelectField from 'material-ui/SelectField';
import MenuItem from 'material-ui/MenuItem';
import RaisedButton from 'material-ui/RaisedButton';


const renderTextField = (
    { input, label, meta: { touched, error }, ...custom },
) => (
    <TextField
        hintText={label}
        type="number"
        floatingLabelText={label}
        errorText={touched && error}
        {...input}
        {...custom}
    />
);

const renderSelectField = (
    { input, label, meta: { touched, error }, children, ...custom },
) => (
    <SelectField
        floatingLabelText={label}
        errorText={touched && error}
        autoWidth={true}
        {...input}
        onChange={(event, index, value) => input.onChange(value)}
        children={children}
        {...custom}
    />
);

let selectedFromNumber = null;
let selectedToNumber = null;
let selectedFromAccount = null;
let selectedToAccount = null;
let enteredAmount =0;
let confirmationNumber = null;


const TransferFormWrapper = styled(Card)`
  padding: 30px 30px 40px;
  @media (max-width: 599px) {
    width: 100%;
    height: calc(200vh - 70px);
    margin-top: 70px;
    box-shadow: none !important;
  }
  @media (min-width: 600px) {
    width: 650px;
    margin: 100px auto;
    box-shadow: 0 3px 1px -2px rgba(0,0,0,.2), 0 2px 2px 0 rgba(0,0,0,.14), 0 1px 5px 0 rgba(0,0,0,.12) !important;
  }
`;

const ButtonsWrapper = styled.div`
  margin-top: 30px;
  text-align: left;
  > a {
    text-decoration: underline;
    color: rgba(0, 0, 0, .7);
`;


const muiTheme = getMuiTheme({
    appBar: {
        height: 70,
        color: lightBlue700
    }
});

class TransferForm extends Component {

    constructor(props) {
        super(props);
        this.nextPage = this.nextPage.bind(this);
        this.previousPage = this.previousPage.bind(this);
        this.state = {
            page: 1,
        };
    }

    previousPage() {
        this.setState({page: this.state.page - 1});
    }

    nextPage() {
        this.setState({page: this.state.page + 1});
    }

    findSelectedAccount(selectedNumber){
        return this.props.transferData.find(function(account){
            return account.number === selectedNumber;
        });
    }

    componentWillMount() {
        this.props.fetchAccounts();
    }



    handleFormSubmit({fromNumber, toNumber, amount}) {
        selectedFromNumber = fromNumber;
        selectedToNumber = toNumber;
        enteredAmount = amount;
        if(this.state.page === 1) {
            selectedFromAccount = this.findSelectedAccount(selectedFromNumber);
            selectedToAccount = this.findSelectedAccount(selectedToNumber);
            this.nextPage();
        }
        else if (this.state.page === 2) {
            this.props.makeTransfer({fromNumber, toNumber, amount});
            this.nextPage();
        }
    }


    getStepContent(stepIndex) {
        switch (stepIndex) {
            case 0:
                return 'Select campaign settings...';
            case 1:
                return 'What is an ad group anyways?';
            case 2:
                return 'This is the bit I really care about!';
            default:
                return 'You\'re a long way from home sonny jim!';
        }
    }

    render() {

        const {handleSubmit, pristine, reset, submitting} = this.props;
        switch (this.state.page) {
            case 1:
                return (
                    <TransferFormWrapper>
                    <form name = 'verify' onSubmit={handleSubmit(this.handleFormSubmit.bind(this))}>
                        <div><h2>Transfers Between My Accounts</h2></div>

                        <div>
                        <Field
                            name="fromNumber"
                            component={renderSelectField}
                            label="From Account"
                        >
                            {
                                _.map(this.props.transferData, account => {
                                    if (account.type === 'Banking' && (account.currencyCode === 'CAD'))
                                        return (
                                            <MenuItem value={account.number}   primaryText={account.type + " | " + account.number + " |  $" + account.currentBalance +  account.currencyCode} />)
                                })
                            }
                        </Field>
                    </div>
                        <div>
                            <Field
                                name="toNumber"
                                component={renderSelectField}
                                label="To Account"
                            >
                                {
                                    _.map(this.props.transferData, account => {
                                        if (account.currencyCode === 'CAD')
                                        return (
                                            <MenuItem value={account.number}   primaryText={account.type + " | " + account.number + " |  $" + account.currentBalance +  account.currencyCode} />)
                                    })
                                }
                            </Field>
                        </div>

                        <div>
                            <Field
                                name="amount"
                                component={renderTextField}
                                label="Transfer Amount"
                            />
                        </div>
                        <ButtonsWrapper>
                        <div>
                            <RaisedButton type="button" disabled={pristine || submitting} onClick={reset}>Cancel</RaisedButton>
                            <RaisedButton type="submit" disabled={pristine || submitting}>Next</RaisedButton>
                        </div>
                        </ButtonsWrapper>


                    </form>
                    </TransferFormWrapper>

                );
                break;

            case 2:
                return (
                    <TransferFormWrapper>
                    <form onSubmit={handleSubmit(this.handleFormSubmit.bind(this))}>
                        <div>
                           <div><h2>Verify Funds Transfer</h2></div>

                            <div>
                                <div><lable><h4>From Account </h4></lable></div>
                                {selectedFromAccount.type} | {selectedFromAccount.number} |  ${selectedFromAccount.currentBalance} {selectedFromAccount.currencyCode}
                            </div>
                            <div>
                                <div><lable><h4>To Account</h4></lable></div>
                                {selectedToAccount.type} | {selectedToAccount.number} |  ${selectedToAccount.currentBalance} {selectedToAccount.currencyCode}
                            </div>

                            <div>
                                <div><lable><h4>Amount Transfered</h4></lable></div>
                                ${enteredAmount} {selectedToAccount.currencyCode}
                            </div>

                            <ButtonsWrapper>
                            <div>
                                <RaisedButton type="button" disabled={pristine || submitting} onClick={this.previousPage}>Previous</RaisedButton>
                                <RaisedButton type="submit" disabled={pristine || submitting}>Submit</RaisedButton>
                            </div>
                            </ButtonsWrapper>
                        </div>
                    </form>
                    </TransferFormWrapper>
                );
                break;
            case 3:
                return (
                    <TransferFormWrapper>
                        <form>
                        <div>
                            <h2> Transfer Confirmation</h2>
                            Transfer is completed successfully. Confirmation ID: {this.props.transferData.referenceNumber}

                            <div>
                                <lable><h4>From Account</h4></lable>
                                {selectedFromAccount.type} | {selectedFromAccount.number} | {this.props.transferData.fromAccountBalance} {selectedFromAccount.currencyCode}
                            </div>

                            <div>
                                <div><lable><h4>To Account</h4></lable></div>
                                {selectedToAccount.type} | {selectedToAccount.number} | {this.props.transferData.toAccountBalance} {selectedToAccount.currencyCode}
                            </div>

                            <div>
                                <div><lable><h4>Amount Transfered</h4></lable></div>
                                ${enteredAmount} {selectedToAccount.currencyCode}
                            </div>
                        </div>
                        <ButtonsWrapper>
                            <div>
                                <RaisedButton type="submit" disabled={pristine || submitting}>More Transfer</RaisedButton>
                            </div>
                        </ButtonsWrapper>
                        </form>
                    </TransferFormWrapper>
                );
        }
    }
}

function validate(values) {

    const errors = {};

    if (!values.fromNumber) {
        errors.fromNumber = "From Account Number is required";
    }
    if (!values.toNumber ) {
        errors.toNumber = "To Account Number is required";
    }
    if (values.fromNumber === values.toNumber){
        errors.toNumber = "From and To Account Number is same";
    }
    if (!values.amount) {
        errors.amount = "Transfer Amount is required";
    }
    return errors;
}

function mapStateToProps(state) {
    return { transferData: state.transferData};
}

//Connect: first argumant is mapStateToProps, second argument is mapDispatchToProps
//redux: first is form config, second is mapStateToProps, third is mapDispatchToProps
export default reduxForm({
    form: "TransferForm",
    validate,
})(connect(mapStateToProps, actions )(TransferForm));
