/**
 * Created by E75536 on 8/21/2017.
 */

import { FETCH_ACCOUNTS,  MAKE_TRANSFER } from '../../config/actionTypes';

export default function( state = [], action ){

    switch (action.type) {
        case FETCH_ACCOUNTS:
            return action.payload;
        case MAKE_TRANSFER:
            return action.payload;
    }
    return state;
}

