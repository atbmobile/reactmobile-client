import React from 'react';
import ReactDOM from 'react-dom';
import { Provider } from 'react-redux';
import { createStore, applyMiddleware } from 'redux';
import { Route, Switch, Redirect } from 'react-router-dom';
import createHistory from 'history/createBrowserHistory';
import { ConnectedRouter, routerMiddleware } from 'react-router-redux';
import reduxThunk from 'redux-thunk';
import promise from "redux-promise";

import './index.css';
import reducers from './app/reducers';
import httpInterceptor from './app/shared/http-interceptor'
import HttpLoader from './app/shared/http-loader.component';
import Auth from './app/features/auth/auth.component';
import RequireAuth from './app/features/auth/require-auth';
import { isAuthenticated } from './app/features/auth/auth.action';
import AccountDetail from './app/features/account/details/account_detail.component';
import AccountSummary from './app/features/account/summary/account_summary.component';
import Transfer from './app/features/transfer/transfer.component';
import TransferFailed from './app/features/transfer/transferfailed.component';
import BillIndex from './app/features/bill/BillPage';

const history = createHistory();
const store = createStore(
  reducers,
  applyMiddleware(routerMiddleware(history), reduxThunk, promise)
);

httpInterceptor.setup(store);

if (sessionStorage.getItem('token')) {
  store.dispatch(isAuthenticated(true));
}

ReactDOM.render(
  <Provider store={store}>
    <ConnectedRouter history={history}>
      <div>
        <HttpLoader />
        <Switch>
          <Route exact path="/" render={() => (<Redirect to="/signin" />)} />
          <Route path="/signin" component={Auth}/>
          <Route path="/signup" component={Auth}/>
          <Route path="/resetpassword" component={Auth}/>
          <Route path="/account/:id" component={RequireAuth(AccountDetail)}/>
          <Route path="/account" component={RequireAuth(AccountSummary)}/>
          <Route path="/transfer" component={RequireAuth(Transfer)}/>
          <Route path="/transferfailed" component={RequireAuth(TransferFailed)}/>
          <Route path="/bill" component={ RequireAuth( BillIndex ) }/>
        </Switch>
      </div>
    </ConnectedRouter>
  </Provider>
  , document.getElementById('root')
);
